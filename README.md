# exercicio_03

Navegação entre páginas e Programação Assíncrona.

## Tarefa 01

- Adapte o código atual para carregar um JSON contendo objetos do tipo Post (https://jsonplaceholder.typicode.com/posts) e exibir em uma ListView.

## Tarefa 02

- Criar uma nova página de detalhes para cada item da ListView e usar o Navigation ao clicar no item. Quando a nova tela for carregada, um novo get deverá ser feito para buscar os dados daquele post específico, ex: Se clicou no item 2 você deve fazer uma requisição para https://jsonplaceholder.typicode.com/posts/2.

## Tarefa 03

- Experimentar diferentes formas de tratamentos de erros de acesso ao HTTP utilizando diferentes URLs, como por exemplo, uma URL inexistente (erro 404), e exiba uma mensagem de erro na tela.